﻿#pragma checksum "..\..\..\Interfaces\GererAgents.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "A17C7E804776EE306C4CC542951D95C0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace fsg_gpao.Interfaces {
    
    
    /// <summary>
    /// GererAgents
    /// </summary>
    public partial class GererAgents : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 51 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBId;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBLogin;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBMotdepasse;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBEmail;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CBService;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CHEtat;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTModifier;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTSupprimer;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTConfirmer;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTAnnuler;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DataGridAgents;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CBFiltres;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBFiltres;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\Interfaces\GererAgents.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MsgInfo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/fsg_gpao;component/interfaces/gereragents.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Interfaces\GererAgents.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TBId = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.TBLogin = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.TBMotdepasse = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.TBEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.CBService = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.CHEtat = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 7:
            this.BTModifier = ((System.Windows.Controls.Button)(target));
            
            #line 76 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTModifier.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTModifier_MouseEnter);
            
            #line default
            #line hidden
            
            #line 76 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTModifier.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTModifier_MouseLeave);
            
            #line default
            #line hidden
            
            #line 76 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTModifier.Click += new System.Windows.RoutedEventHandler(this.BTModifier_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.BTSupprimer = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTSupprimer.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTSupprimer_MouseEnter);
            
            #line default
            #line hidden
            
            #line 79 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTSupprimer.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTSupprimer_MouseLeave);
            
            #line default
            #line hidden
            
            #line 79 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTSupprimer.Click += new System.Windows.RoutedEventHandler(this.BTSupprimer_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.BTConfirmer = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTConfirmer.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTConfirmer_MouseEnter);
            
            #line default
            #line hidden
            
            #line 82 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTConfirmer.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTConfirmer_MouseLeave);
            
            #line default
            #line hidden
            
            #line 82 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTConfirmer.Click += new System.Windows.RoutedEventHandler(this.BTConfirmer_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.BTAnnuler = ((System.Windows.Controls.Button)(target));
            
            #line 85 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTAnnuler.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTAnnuler_MouseEnter);
            
            #line default
            #line hidden
            
            #line 85 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTAnnuler.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTAnnuler_MouseLeave);
            
            #line default
            #line hidden
            
            #line 85 "..\..\..\Interfaces\GererAgents.xaml"
            this.BTAnnuler.Click += new System.Windows.RoutedEventHandler(this.BTAnnuler_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.DataGridAgents = ((System.Windows.Controls.DataGrid)(target));
            
            #line 100 "..\..\..\Interfaces\GererAgents.xaml"
            this.DataGridAgents.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.DataGridAgents_MouseUp);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 111 "..\..\..\Interfaces\GererAgents.xaml"
            ((System.Windows.Controls.Expander)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Expander_MouseDown);
            
            #line default
            #line hidden
            
            #line 111 "..\..\..\Interfaces\GererAgents.xaml"
            ((System.Windows.Controls.Expander)(target)).MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.Expander_MouseDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.CBFiltres = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.TBFiltres = ((System.Windows.Controls.TextBox)(target));
            
            #line 123 "..\..\..\Interfaces\GererAgents.xaml"
            this.TBFiltres.KeyUp += new System.Windows.Input.KeyEventHandler(this.TBFiltres_KeyUp);
            
            #line default
            #line hidden
            return;
            case 15:
            this.MsgInfo = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

