﻿#pragma checksum "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "18C8D97A9ADF575E55FBDACE35B0C6F5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace fsg_gpao.Interfaces {
    
    
    /// <summary>
    /// AJouterGroupeDeTravail
    /// </summary>
    public partial class AJouterGroupeDeTravail : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 34 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBNom;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBPrenom;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBIdAtelier;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CBAtelier;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBDate;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TBRemarque;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label msg;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTAjouter;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTQuitter;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTConfirmer;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTAnnuler;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/fsg_gpao;component/interfaces/ajoutergroupedetravail.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TBNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.TBPrenom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.TBIdAtelier = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.CBAtelier = ((System.Windows.Controls.ComboBox)(target));
            
            #line 38 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.CBAtelier.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CBAtelier_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.TBDate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.TBRemarque = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.msg = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.BTAjouter = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTAjouter.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTAjouter_MouseEnter);
            
            #line default
            #line hidden
            
            #line 55 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTAjouter.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTAjouter_MouseLeave);
            
            #line default
            #line hidden
            
            #line 55 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTAjouter.Click += new System.Windows.RoutedEventHandler(this.BTAjouter_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.BTQuitter = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTQuitter.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTQuitter_MouseEnter);
            
            #line default
            #line hidden
            
            #line 58 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTQuitter.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTQuitter_MouseLeave);
            
            #line default
            #line hidden
            
            #line 58 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTQuitter.Click += new System.Windows.RoutedEventHandler(this.BTQuitter_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.BTConfirmer = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTConfirmer.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTConfirmer_MouseEnter);
            
            #line default
            #line hidden
            
            #line 62 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTConfirmer.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTConfirmer_MouseLeave);
            
            #line default
            #line hidden
            
            #line 62 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTConfirmer.Click += new System.Windows.RoutedEventHandler(this.BTConfirmer_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.BTAnnuler = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTAnnuler.MouseEnter += new System.Windows.Input.MouseEventHandler(this.BTAnnuler_MouseEnter);
            
            #line default
            #line hidden
            
            #line 65 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTAnnuler.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BTAnnuler_MouseLeave);
            
            #line default
            #line hidden
            
            #line 65 "..\..\..\Interfaces\AJouterGroupeDeTravail.xaml"
            this.BTAnnuler.Click += new System.Windows.RoutedEventHandler(this.BTAnnuler_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

